#!/usr/bin/python

import pygame
import time
from pygame.locals import *

import RPi.GPIO as GPIO
import time
import math
import thread
import multiprocessing
from multiprocessing import Process
import random
import math
import numpy
import os
import datetime

font_a = "/usr/share/fonts/truetype/roboto/Roboto-Bold.ttf"
font_b = "/usr/share/fonts/truetype/roboto/Roboto-Bold.ttf"
font_c = "/usr/share/fonts/truetype/roboto/Roboto-Bold.ttf"

#Pygame expects 16 bit integers defining the X and Y coordinates sampled from the sine wave
bits = 16

#the number of channels specified here is NOT 
#the channels talked about here http://www.pygame.org/docs/ref/mixer.html#pygame.mixer.get_num_channels


#We're going to play 44100 samples per second
pygame.mixer.pre_init(44100, -bits, 2)
pygame.init()
pygame.mixer.init()

#Make sure we take samples at the same rate we want to play them
sample_rate = 44100

note_duration = 0.125

def generate_sound(frequency, duration):
	#Figure out how many samples we need to take, use numpy library to create teh sample array
	n_samples = int(round(duration*sample_rate))
	buf = numpy.zeros((n_samples, 2), dtype = numpy.int16)
	max_sample = 2**(bits - 1) - 1

	for s in range(n_samples):
		t = float(s)/sample_rate    # time in seconds
		frequency_l = frequency;
		frequency_r = frequency;
		#grab the x-coordinate of the sine wave at a given time, while constraining the sample to what our mixer is set to with "bits"
		buf[s][0] = int(round(max_sample*math.sin(2*math.pi*frequency_l*t)))    # left
		buf[s][1] = int(round(max_sample*math.sin(2*math.pi*frequency_r*t)))    # right
	#Play the sound
	sound = pygame.sndarray.make_sound(buf)
	return sound;

our_sounds=[generate_sound(220, note_duration),
generate_sound(261.64 , note_duration),
generate_sound(261.6 , note_duration),
generate_sound(293.66 , note_duration),
generate_sound(329.63, note_duration),
generate_sound(349.23 , note_duration),
generate_sound(392  , note_duration),
generate_sound(440, note_duration),
generate_sound(493.88, note_duration),
generate_sound(523.25 , note_duration),
generate_sound(587.33 , note_duration),
generate_sound(659.26 , note_duration),
generate_sound(698.46, note_duration),
generate_sound(783.99 , note_duration),
generate_sound(880 , note_duration),
generate_sound(987.77 , note_duration),
generate_sound(1046.50 , note_duration),
generate_sound(1174.66 , note_duration),
generate_sound(1318.51 , note_duration),
generate_sound(1396.91  , note_duration),
generate_sound(1567.98 , note_duration),
generate_sound(1760, note_duration)]  	

our_colours=[(204,0,0),
(204,0,51),
(204,0,102),
(204,0,153),
(204,0,204),
(204,0,255),
(204,51,0),
(204,51,51),
(204,51,102),
(204,51,153),
(204,51,204),
(204,51,255),
(204,51,255),
(204,102,51),
(204,102,102),
(204,102,153),
(204,102,204),
(204,102,255),
(204,153,0),
(204,153,51),
(204,153,102),
(204,153,153)]  	


our_notes=['A3',
'B3',
'C3',
'D3',
'E3',
'F3',
'G3',
'A4',
'B4',
'C4',
'D4',
'E4',
'F4',
'G4',
'A5',
'B5',
'C5',
'D5',
'E5',
'F5',
'G5',
'A5']

def play(sound):
	chan1 = pygame.mixer.find_channel()
        for x in range(0, random.randint(0, 50)):
                chan1.queue(our_sounds[int(sound)])

	chan2 = pygame.mixer.find_channel()	
        for x in range(0, random.randint(0, 50)):
                chan2.queue(our_sounds[int(sound + random.randint(2, 4))])

#	chan5 = pygame.mixer.find_channel()
#	chan5.queue(our_sounds[int(sound + 5)])
	
	chan3 = pygame.mixer.find_channel()
        for x in range(0, random.randint(0, 50)):
                chan3.queue(our_sounds[int(sound + 5)])
	
	chan4 = pygame.mixer.find_channel()
        for x in range(0, random.randint(0, 50)):
                chan4.queue(our_sounds[int(sound + 8)])
	
	is_ninth = random.randint(7,9)
	if is_ninth == 9:
		chan5 = pygame.mixer.find_channel()
                for x in range(0, random.randint(0, 50)):
                        chan5.queue(our_sounds[int(sound + 9)])
        else:
		chan5 = pygame.mixer.find_channel()
                for x in range(0, random.randint(0, 50)):
                        
                        chan5.queue(our_sounds[int(sound + 8)])                                        
#Pitch meaure START
#Use GPIO pin numbers to address the pins
GPIO.setmode(GPIO.BCM)

#Convenience handles for the pin numbers
TRIG_pitch = 23 
ECHO_pitch = 24

GPIO.setup(TRIG_pitch,GPIO.OUT)
GPIO.setup(ECHO_pitch,GPIO.IN)
	
def main():
	
	# Initialise screen
	pygame.init()
	screen = pygame.display.set_mode((0, 0), pygame.RESIZABLE)
	pygame.display.set_caption('The Synthetic Synesthesic Sonic Spatulas welcome YOU!')

	# Fill background
	background1 = pygame.Surface((screen.get_width(), screen.get_height() / 2))
	background1 = background1.convert()
	background1.fill((255,255,255))

	background2 = pygame.Surface((screen.get_width(), screen.get_height() / 2))
	background2 = background2.convert()
	background2.fill((255,255,255))

	# Blit everything to the screen
	screen.blit(background1, (0, 0))	
	screen.blit(background2, (0, screen.get_height() / 2))

	pygame.display.flip()
        time.sleep(0.5)
        
	draw(font_a, (255,255,255,0), (0,0,0), "Sonic", screen, 0, background1, True, 244)
        time.sleep(0.5)
	draw(font_a, (255,255,255,0), (0,0,0), "Sonic", screen, 0, background2, False, 72)
        time.sleep(0.5)
	draw(font_a, (255,255,255,0), (0,0,0), "Synesthesic", screen, 0, background1, True, 244)
        time.sleep(0.5)
	draw(font_a, (255,255,255,0), (0,0,0), "Sonic Synesthesic", screen, 0, background2, False, 72)
        time.sleep(0.5)
        draw(font_a, (255,255,255,0), (0,0,0), "Synthetic", screen, 0, background1, True, 244)
        time.sleep(0.5)
	draw(font_a, (255,255,255,0), (0,0,0), "Sonic Synesthesic Synthetic", screen, 0, background2, False, 72)
        time.sleep(0.5)
	draw(font_a, (255,255,255,0), (0,0,0), "Spatulas", screen, 0, background1, True, 244)
        time.sleep(0.5)
	draw(font_a, (255,255,255,0), (0,0,0), "Sonic Synesthesic Synthetic Spatulas", screen, 0, background2, False, 72)
        time.sleep(0.5)
	draw(font_a, (255,255,255,0), (0,0,0), "S.S.S.S.", screen, 0, background1, True, 244)
        time.sleep(0.1)
	draw(font_a, (255,255,255,0), (0,0,0), "", screen, 0, background1, True, 244)
        time.sleep(0.1)
	draw(font_a, (255,255,255,0), (0,0,0), "S.S.P.S.", screen, 0, background1, True, 244)
        time.sleep(0.1)
	draw(font_a, (255,255,255,0), (0,0,0), "", screen, 0, background1, True, 244)
        time.sleep(0.1)
	draw(font_a, (255,255,255,0), (0,0,0), "S.S.T.S.", screen, 0, background1, True, 244)
        time.sleep(0.1)
	draw(font_a, (255,255,255,0), (0,0,0), "", screen, 0, background1, True, 244)
        time.sleep(0.1)
	draw(font_a, (255,255,255,0), (0,0,0), "S.S.S.S.", screen, 0, background1, True, 244)
        time.sleep(0.1)
	draw(font_a, (255,255,255,0), (0,0,0), "", screen, 0, background1, True, 244)
        time.sleep(0.1)
	draw(font_a, (255,255,255,0), (0,0,0), "S.S.Q.S.", screen, 0, background1, True, 244)
        time.sleep(0.1)
	draw(font_a, (255,255,255,0), (0,0,0), "", screen, 0, background1, True, 244)
        time.sleep(0.1)
	draw(font_a, (255,255,255,0), (0,0,0), "Q.S.S.S.", screen, 0, background1, True, 244)
        time.sleep(0.1)
	draw(font_a, (255,255,255,0), (0,0,0), "", screen, 0, background1, True, 244)
        time.sleep(0.1)
	draw(font_a, (255,255,255,0), (0,0,0), "S.O.S.!.", screen, 0, background1, True, 244)
        time.sleep(0.1)
	draw(font_a, (255,255,255,0), (0,0,0), "", screen, 0, background1, True, 244)
	draw(font_a, (255,255,255,0), (0,0,0), "Would Like to Apologise in Advance.", screen, 0, background2, False, 64)
        time.sleep(0.1)
	draw(font_a, (255,255,255,0), (0,0,0), "S.S.S.S.", screen, 0, background1, True, 244)
        time.sleep(0.1)
	draw(font_a, (255,255,255,0), (0,0,0), "", screen, 0, background1, True, 244)
        time.sleep(0.1)
	draw(font_a, (255,255,255,0), (0,0,0), "S.O.S.!.", screen, 0, background1, True, 244)
        time.sleep(0.1)
	draw(font_a, (255,255,255,0), (0,0,0), "", screen, 0, background1, True, 244)
        time.sleep(0.1)
	draw(font_a, (255,255,255,0), (0,0,0), "S.S.S.S.", screen, 0, background1, True, 244)
        time.sleep(0.1)
	draw(font_a, (255,255,255,0), (0,0,0), "", screen, 0, background1, True, 244)
        time.sleep(0.1)
	draw(font_a, (255,255,255,0), (0,0,0), "S.S.S.S.", screen, 0, background1, True, 244)
        time.sleep(0.1)
	draw(font_a, (255,255,255,0), (0,0,0), "", screen, 0, background1, True, 244)
        time.sleep(0.1)
	draw(font_a, (255,255,255,0), (0,0,0), "S.S.S.S.", screen, 0, background1, True, 244)
        time.sleep(0.1)
	draw(font_a, (255,255,255,0), (0,0,0), "", screen, 0, background1, True, 244)
        time.sleep(0.1)
	draw(font_a, (255,255,255,0), (0,0,0), "S.S.S.S.", screen, 0, background1, True, 244)
        time.sleep(0.1)
	draw(font_a, (255,255,255,0), (0,0,0), "", screen, 0, background1, True, 244)
        time.sleep(0.1)
	draw(font_a, (255,255,255,0), (0,0,0), "S.S.S.S.", screen, 0, background1, True, 244)
        time.sleep(0.1)
	draw(font_a, (255,255,255,0), (0,0,0), "", screen, 0, background1, True, 244)
        time.sleep(0.1)
	draw(font_a, (255,255,255,0), (0,0,0), "S.S.S.S.", screen, 0, background1, True, 244)
        time.sleep(3.5)


######
######
	note_string = "";
	while 0 == 0:
		t_start = datetime.datetime.now()
		#  print "start time: " + int(t_start.total_seconds() * 1000)
		GPIO.output(TRIG_pitch, False)
		time.sleep(0.12)

		GPIO.output(TRIG_pitch, True)

		time.sleep(0.00001)

		GPIO.output(TRIG_pitch, False)

		#To be honest I'l still trying to figure ut exactly how this works :-)

		while GPIO.input(ECHO_pitch)==0:
			pulse_start_pitch = time.time()

		while GPIO.input(ECHO_pitch)==1:
			pulse_end_pitch = time.time()

		pulse_duration_pitch = pulse_end_pitch - pulse_start_pitch
		distance_pitch = pulse_duration_pitch * 17150
		distance_pitch = math.floor(round(distance_pitch, 0)) 

		t_measure_end = datetime.datetime.now() 	
		delta_measure = t_start - t_measure_end
#		print "time to measure         :" + str(delta_measure.total_seconds() * 1000)
                time_to_sleep = (145000 + (delta_measure.total_seconds() * 1000 * 1000)) / 1000000
                if (time_to_sleep >= 0):
                        time.sleep(time_to_sleep)
                
		t_play_start = datetime.datetime.now()  

		if distance_pitch <= 60 and distance_pitch >= 0:
			distance_pitch = math.floor((distance_pitch) / 4)
                        note_string = our_notes[int(distance_pitch)] + " " + note_string  
                        if (len(note_string) >= 42):
                                note_string = note_string[0:39] 
	
			#  try:
			#    0 == 0 
			 # p = Process(target=play, args = (frequency, 0.25, 0, ))
#			print int(distance_pitch - 1)
			draw(font_a, (0,0,0), our_colours[int(distance_pitch)], note_string, screen, 0, background1, True, 88);
			draw(font_a, our_colours[int(distance_pitch)], (0,0,0), our_notes[int(distance_pitch)], screen, 0, background2, False, 176);
			thread.start_new_thread(play, (int(distance_pitch - 1),))

		t_play_end = datetime.datetime.now()  

		t_end = datetime.datetime.now()

		#  t_start = int(t_start.total_seconds() * 1000)
		#  t_end = int(t_end.total_seconds() * 1000)
		delta = t_start - t_end
		delta_play = t_play_start - t_play_end

		
#		print "Time to play          : " + str(delta_play.total_seconds() * 1000)

		#  print "start time: " + str(t_end)
#		print "Actual measuring time: " + str(delta.total_seconds() * 1000)
		time_to_sleep = (250000 + (delta.total_seconds() * 1000 * 1000)) / 1000000
#		print "Time to sleep         : " + str(time_to_sleep)
                if (time_to_sleep >= 0):
                        time.sleep(time_to_sleep)
		t_real_end = datetime.datetime.now()
		delta = t_start - t_real_end

#		print "total time taken:     : " + str(delta.total_seconds() * 1000)  
#		print "sensor 1 measurement  : " + str(distance_pitch)

#		print "----"

		# Event loop
#		while 1:
#			for event in pygame.event.get():
#				if event.type == QUIT:
#					return

#			screen.blit(background1, (0, 0))
#			screen.blit(background2, (0, screen.get_height() / 2))
#			pygame.display.flip()

def draw(font, text_colour, background_colour, the_text, screen, offset, background, is_top, font_size):
	# Fill background
	#background = pygame.Surface(screen.get_size())
	#background = background.convert()
	background.fill(background_colour)

	# Display some text
	font = pygame.font.Font(font, font_size)
	text = font.render(the_text, 1, text_colour)
	textpos = text.get_rect()
	textpos.centerx = background.get_rect().left
	textpos.centery = background.get_rect().top
	background.blit(text, (textpos.centerx, textpos.centery))

	# Blit everything to the screen
	if is_top:
		screen.blit(background, (0, 0))
	if not is_top:
		screen.blit(background, (0, screen.get_height() / 2))

	pygame.display.flip() 



if __name__ == '__main__': main()
