#!/usr/bin/python

import pygame
import time
from pygame.locals import *

import RPi.GPIO as GPIO
import time
import math
import thread
import multiprocessing
from multiprocessing import Process
import random
import math
import numpy
import os
import datetime

major_scale = "2-2-1-2-2-2-1"
minor_scale = "2-1-2-2-1-2-2"
harmonic_minro_scale = "2-1-2-2-1-3-1"
melodic_minor = "2-1-2-2-2-2-1"

key = "Amaj"
root = "220Hz"
note_length = [0.125, 0.25, 5, 0.75, 1]

current_note_length = note_length[3]

#[2, 4, 6, 9, 6, 4, 2, 4, 6, 9, 6, 4, 0, 4, 7, 9, 4, 7, 0, 4, 7, 9, 4, 7, 5, 12, 7, 9, 7, 12,     5, 12, 7, 9, 7, 12, 6, 11, 8, 10, 8, 11, 6, 11, 8, 10, 8, 11]

chains= [(
("_00_", 4,  9,  11, 6,  7,  0,  5,  12, 8, 10, 0, 2),
("_0_",  1,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0),
("_2_",  1,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0),
("_4_",  0,  0,  0,  1,  2,  1,  2,  0,  0,  0, 1, 1),
("_6_",  1,  1,  1,  0,  0,  0,  0,  0,  0,  0, 0, 0),
("_9_",  1,  1,  0,  0,  1,  0,  0,  0,  0,  0, 0, 0),
("_0_",  1,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0),
("_7_",  1,  2,  0,  0,  0,  0,  1,  1,  0,  0, 0, 0),
("_5_",  1,  0,  0,  1,  1,  0,  0,  1,  1,  0, 0, 0),
("_12_", 1,  0,  0,  1,  1,  0,  0,  0,  0,  0, 0, 0),
("_11_", 0,  0,  0,  0,  1,  0,  0,  0,  1,  0, 0, 0),
("_8_",  0,  0,  1,  0,  0,  0,  0,  0,  0,  1, 0, 0),
("_10_", 1,  0,  0,  0,  0,  0,  0,  0,  1,  0, 0, 0)
  ),

(
("_0__0_",  4, 9, 11, 6, 7, 0, 5, 12, 8, 10, 0),
("_6__9_",  0, 0, 0,  1, 0, 0, 0, 0,  0,  0, 0),
("_2__4_",  0, 0, 0,  1, 0, 0, 0, 0,  0,  0, 0),
("_6__4_",  0, 0, 0,  0, 0, 0, 0, 0,  0,  0, 1),
("_4__2_",  1, 0, 0,  0, 0, 0, 0, 0,  0,  0, 0),
("_4__6_",  0, 1, 0,  0, 0, 0, 0, 0,  0,  0, 0),
("_9__6_",  1, 0, 0,  0, 0, 0, 0, 0,  0,  0, 0),
("_4__0_",  1, 0, 0,  0, 0, 0, 0, 0,  0,  0, 0),
("_0__4_",  0, 0, 0,  0, 2, 0, 0, 0,  0,  0, 0),
("_4__5_",  0, 0, 0,  0, 2, 0, 0, 0,  0,  0, 0),
("_4__7_",  0, 1, 0,  0, 0, 0, 0, 1,  0,  0, 0),
("_7__0_",  1, 0, 0,  0, 0, 0, 0, 0,  0,  0, 0),
("_7__9_",  1, 0, 0,  0, 1, 0, 0, 0,  0,  0, 0),
("_9__4_",  0, 0, 0,  0, 1, 0, 0, 0,  0,  0, 0),
("_7__5_",  0, 0, 0,  0, 0, 0, 0, 1,  0,  0, 0),
("_5__12_", 0, 0, 0,  0, 1, 0, 0, 0,  0,  0, 0),
("_12__7_", 0, 1, 0,  0, 0, 0, 0, 0,  0,  0, 0),
("_9__7_",  0, 0, 0,  0, 0, 0, 0, 1,  0,  0, 0),
("_7__12_", 0, 0, 0,  1, 0, 0, 1, 0,  0,  0, 0),
("_12__6_", 0, 0, 1,  0, 0, 0, 0, 0,  0,  0, 0),
("_6__11_", 0, 0, 0,  0, 0, 0, 0, 0,  1,  0, 0),
("_11__8_", 0, 0, 0,  0, 0, 0, 1, 0,  0,  1, 0),
("_8__10_", 0, 0, 0,  0, 0, 0, 0, 0,  1,  0, 0),
("_10__8_", 1, 0, 1,  0, 0, 0, 0, 0,  0,  0, 0),
("_8__11_", 0, 0, 0,  0, 0, 0, 0, 0,  0,  1, 0)

  ),
         (
("_0__0_",      4, 9, 11, 6, 7, 0, 5, 12, 8, 10, 0),
("_8__10__8_",  0, 0, 1,  0, 0, 0, 0, 0,  0,  0, 0),
("_8__11__10_", 1, 0, 0,  0, 0, 0, 0, 0,  0,  0, 0),
("_12__7__9_",  0, 0, 0,  0, 1, 0, 0, 0,  0,  0, 0),
("_2__4__6_",   0, 1, 0,  0, 0, 0, 0, 0,  0,  0, 0)


  )]

beatz = chains= [(
("_00_", 4,  9,  11, 6,  7,  0,  5,  12, 8, 10, 0, 2),
("_0_",  1,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0),
("_2_",  1,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0),
("_4_",  0,  0,  0,  1,  2,  1,  2,  0,  0,  0, 1, 1),
("_6_",  1,  1,  1,  0,  0,  0,  0,  0,  0,  0, 0, 0),
("_9_",  1,  1,  0,  0,  1,  0,  0,  0,  0,  0, 0, 0),
("_0_",  1,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0),
("_7_",  1,  2,  0,  0,  0,  0,  1,  1,  0,  0, 0, 0),
("_5_",  1,  0,  0,  1,  1,  0,  0,  1,  1,  0, 0, 0),
("_12_", 1,  0,  0,  1,  1,  0,  0,  0,  0,  0, 0, 0),
("_11_", 0,  0,  0,  0,  1,  0,  0,  0,  1,  0, 0, 0),
("_8_",  0,  0,  1,  0,  0,  0,  0,  0,  0,  1, 0, 0),
("_10_", 1,  0,  0,  0,  0,  0,  0,  0,  1,  0, 0, 0)
  )]

     

font_a = "/usr/share/fonts/truetype/roboto/Roboto-Bold.ttf"
font_b = "/usr/share/fonts/truetype/roboto/Roboto-Bold.ttf"
font_c = "/usr/share/fonts/truetype/roboto/Roboto-Bold.ttf"

#Pygame expects 16 bit integers defining the X and Y coordinates sampled from the sine wave
bits = 16

#the number of channels specified here is NOT 
#the channels talked about here http://www.pygame.org/docs/ref/mixer.html#pygame.mixer.get_num_channels


#We're going to play 44100 samples per second
pygame.mixer.pre_init(44100, -bits, 2)
pygame.init()
pygame.mixer.init()

#Make sure we take samples at the same rate we want to play them
sample_rate = 44100

note_duration = 0.250


def generate_sound(frequency, duration):
	#Figure out how many samples we need to take, use numpy library to create teh sample array
	n_samples = int(round(duration*sample_rate))
	buf = numpy.zeros((n_samples, 2), dtype = numpy.int16)
	max_sample = 2**(bits - 1) - 1

	for s in range(n_samples):
		t = float(s)/sample_rate    # time in seconds
		frequency_l = frequency;
		frequency_r = frequency;
		#grab the x-coordinate of the sine wave at a given time, while constraining the sample to what our mixer is set to with "bits"
		buf[s][0] = int(round(max_sample*math.sin(2*math.pi*frequency_l*t)))    # left
		buf[s][1] = int(round(max_sample*math.sin(2*math.pi*frequency_r*t)))    # right
	#Play the sound
	sound = pygame.sndarray.make_sound(buf)
	return sound;

our_sounds=[generate_sound(220, note_duration),
generate_sound(246.94 , note_duration),
generate_sound(261.63 , note_duration),
generate_sound(293.66 , note_duration),
generate_sound(329.63, note_duration),
generate_sound(349.23 , note_duration),
generate_sound(392  , note_duration),
generate_sound(440, note_duration),
generate_sound(493.88, note_duration),
generate_sound(523.25 , note_duration),
generate_sound(587.33 , note_duration),
generate_sound(659.25 , note_duration),
generate_sound(698.46, note_duration),
generate_sound(783.99 , note_duration),
generate_sound(880 , note_duration),
generate_sound(987.77 , note_duration),
generate_sound(1046.50 , note_duration),
generate_sound(1174.66 , note_duration),
generate_sound(1318.51 , note_duration),
generate_sound(1396.91  , note_duration),
generate_sound(1567.98 , note_duration)]

our_colours=[(204,0,0),
(204,0,51),
(204,0,102),
(204,0,153),
(204,0,204),
(204,0,255),
(204,51,0),
(204,51,51),
(204,51,102),
(204,51,153),
(204,51,204),
(204,51,255),
(204,51,255),
(204,102,51),
(204,102,102),
(204,102,153),
(204,102,204),
(204,102,255),
(204,153,0),
(204,153,51),
(204,153,102),
(204,153,153)]  	


our_notes=['A3',
'B3',
'C3',
'D3',
'E3',
'F3',
'G3',
'A4',
'B4',
'C4',
'D4',
'E4',
'F4',
'G4',
'A5',
'B5',
'C5',
'D5',
'E5',
'F5',
'G5',
'A5']

def play(sound):
	chan1 = pygame.mixer.find_channel()
        chan1.queue(our_sounds[int(sound)])
        #chan1.fadeout(250)
#        chan1.queue(our_sounds[int(sound1)])

#	chan2 = pygame.mixer.find_channel()	
#        for x in range(0, random.randint(0, 50)):
#        chan2.queue(our_sounds[int(sound + random.randint(3, 4))])

#        chan2.queue(our_sounds[int(sound1 + random.randint(3, 4))])

	chan5 = pygame.mixer.find_channel()
	chan5.queue(our_sounds[int(sound + 5)])
	#chan5.fadeout(250)
#	chan5.queue(our_sounds[int(sound1 + 5)])
	
#	chan3 = pygame.mixer.find_channel()
#        for x in range(0, random.randint(0, 50)):
#                chan3.queue(our_sounds[int(sound + 5)])
	
	chan4 = pygame.mixer.find_channel()
#        for x in range(0, random.randint(0, 50)):
        chan4.queue(our_sounds[int(sound + 7)])
        #chan4.fadeout(250)
#        chan4.queue(our_sounds[int(sound1 + 7)])

        time.sleep(0.250)                        
#Pitch meaure START
#Use GPIO pin numbers to address the pins
GPIO.setmode(GPIO.BCM)

#Convenience handles for the pin numbers
TRIG_pitch = 23 
ECHO_pitch = 24

GPIO.setup(TRIG_pitch,GPIO.OUT)
GPIO.setup(ECHO_pitch,GPIO.IN)

def get_next(chain):
  (order, row) = get_highest_order_with_match(chain)
#  print "Order=" + str(order)

#  print "Row=" + str(row)
#  print chains
  column = get_winning_column(int(order), int(row))  
#  print "Column=" + str(column)
  note = chains[order][0][column]            
#  print "Note=" + str(note)
  return note
          
def get_winning_column(order, row):
  odds = chains[order][row]
  all_poss = 0
  for p in range(1, len(odds)):
    all_poss = all_poss + odds[p]

  print "all_poss=" + str(all_poss)	
  seed = int(math.floor(random.random() * (all_poss + 1)))
  winner_number = 0
  lowest_match = 0
  range_start = 0
  range_end = 0
  print "==="
  print chains[order][row]
  print "seed= " + str(seed)  
  for x in range(1, len(odds)):
    range_end = range_end + odds[x]
    print str(range_start) + "-" + str(range_end)
    if (seed >= range_start and seed <= range_end and odds[x] != 0):
      return x
    range_start = range_start + odds[x]
  
def get_highest_order_with_match(chain):
  chain_string = ""
  for x in range(0, len(chain)):
    chain_string = chain_string + str(chain[x])
	
  #if (len(chain_string) <= 10):
  #  chain_string = "XXXXXXXXXX" + chain_string
 
  for i in range(len(chains) - 1, -1, -1):
    for j in range(len(chains[i]) - 1, 0, -1):

      print "this guy " + str(chains[i][j][0])
      print len(str(chains[i][j][0]))
      temp_chain_string = chain_string[len(chain_string)-len(str(chains[i][j][0])):len(chain_string)]
      print str(temp_chain_string) + " " +  str(chains[i][j][0])
      if (temp_chain_string == str(chains[i][j][0])):
        return i, j  

  return "", ""   		

def get_index(value, tupple):
  return "foo2"
	
   
def main():
  chain = []
  note = 2
   

  current_note_length = note_length[0]  
  #chain = [2, 4, 6, 9, 6, 4, 2, 4, 6, 9, 6, 4, 0, 4, 7, 9, 4, 7, 0, 4, 7, 9, 4, 7, 5, 12, 7, 9, 7, 12,     5, 12, 7, 9, 7, 12, 6, 11, 8, 10, 8, 11, 6, 11, 8, 10, 8, 11]
  for t in chain:
    play(t, 0)

  chain = []
  for i in range(0, 10000):
    t_start = datetime.datetime.now()
    chain.append("_" + str(note) + "_")

#    print "==="
    print note
    note = get_next(chain)

    if (note <= 10):
      play(note)          

#    note1 = get_next(chain1)

#    if (note1 <= 11):
#      play(note1)
        	
	#silence
#    if (note == 11):
#      time.sleep(0.125)
	#Increase note length
#    if (note == 10):
#      if ((float(current_note_length) + 0.250) <= 1.500):      
#        current_note_length = float(current_note_length) + 0.250
	#Decrease note length
 #   if (note == 11):
#      if ((float(current_note_length) - 0.250) == 0.000):      
        
#		current_note_length = float(current_note_length) - 0.250
  
	#Change key
#    if (note == 11):
#      a = ""	  

#    print current_note_length	  
	
    chain_string = ""
    for x in range(0, len(chain)):
      note_string = str(chain[x])  
	  #Tempo down
      note_string = note_string.replace('10', 'Downtempo')  
      #Key change	  
      note_string = note_string.replace('11', 'KeyChange')        
      note_string = note_string.replace('1', 'I')
      note_string = note_string.replace('2', 'II')
      note_string = note_string.replace('3', 'III')
      note_string = note_string.replace('4', 'IV')
      note_string = note_string.replace('5', 'V')
      note_string = note_string.replace('6', 'VI')
      note_string = note_string.replace('7', 'VII')
      #Random tempo change     
      note_string = note_string.replace('8', 'VIII')
      #Tempo up
      note_string = note_string.replace('9', 'Silence')

      note_string = note_string + " (" + str(current_note_length) + ")"
      chain_string = chain_string + " " + note_string
#    print chain_string
    t_end = datetime.datetime.now()
    delta_measure = t_start - t_end
    time_to_sleep = (275000 + (delta_measure.total_seconds() * 1000 * 1000)) / 1000000
    if (time_to_sleep >= 0):
      time.sleep(time_to_sleep)
#    print str(delta_measure.total_seconds() * 1000)
    
    

def draw(font, text_colour, background_colour, the_text, screen, offset, background, is_top, font_size):
	# Fill background
	#background = pygame.Surface(screen.get_size())
	#background = background.convert()
	background.fill(background_colour)

	# Display some text
	font = pygame.font.Font(font, font_size)
	text = font.render(the_text, 1, text_colour)
	textpos = text.get_rect()
	textpos.centerx = background.get_rect().left
	textpos.centery = background.get_rect().top
	background.blit(text, (textpos.centerx, textpos.centery))

	# Blit everything to the screen
	if is_top:
		screen.blit(background, (0, 0))
	if not is_top:
		screen.blit(background, (0, screen.get_height() / 2))

	pygame.display.flip() 



if __name__ == '__main__': main()
