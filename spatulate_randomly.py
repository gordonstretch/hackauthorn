#!/usr/bin/python

import pygame
import time
from pygame.locals import *

import RPi.GPIO as GPIO
import time
import math
import thread
import multiprocessing
from multiprocessing import Process
import random
import math
import numpy
import os
import datetime

major_scale = "2-2-1-2-2-2-1"
minor_scale = "2-1-2-2-1-2-2"
harmonic_minro_scale = "2-1-2-2-1-3-1"
melodic_minor = "2-1-2-2-2-2-1"

key = "Amaj"
root = "220Hz"
note_length = [0.125, 0.25, 5, 0.75, 1]

current_note_length = note_length[3]

chains = [(
(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10),
(1, 0 ,0 ,0 ,13 ,5 ,0 ,22, 0, 0, 22),
(2, 0 ,0 ,41 ,44 ,1 ,1 ,1, 1, 0, 12),
(3, 0 ,0 ,10 ,520 ,2 ,1 ,1, 50, 0, 22),
(4, 25 ,0 ,22 ,10 ,40 ,2 ,25, 0, 0, 15),
(5, 30 ,0 ,31 ,1 ,0 ,45 ,4, 1, 0, 21),
(6, 1 ,1 ,0 ,0 ,0 ,0 ,59, 2, 0, 33),
(7, 50 ,1 ,0 ,0 ,5 ,1 ,0, 60, 0, 0),
(8, 0 ,0 ,14 ,18 ,1 ,2 ,1, 0, 65, 1),
(9, 35 ,0 ,1 ,0 ,40 ,3 ,1, 1, 15, 1),
(10, 0 ,0 ,0 ,51 ,55 ,1 ,0, 1, 70, 11)),


((0, 3,  4, 8, 7,  5,  2, 1),
(15, 33, 3, 4, 7,  5,  2, 22),
(57, 1,  6, 3, 7,  5,  2, 22),
(71, 0,  2, 8, 37,  5,  2, 1),
(12, 0,  2, 8, 7,  5,  2, 1),
(56, 4,  2, 4, 7,  5,  2, 1),
(45, 26, 6, 3, 7,  5,  2, 25),
(93, 0,  52, 8, 7,  52, 22, 1),
(58, 4,  2, 4, 50,  5,  2, 1),
(38, 1,  6, 3, 7,  85,  2, 1),
(18, 0,  2, 8, 7,  5,  2, 1),
(81, 4,  82, 4, 67, 5,  2, 22),
(83, 1,  86, 3, 7,  5,  2, 22),
(85, 0,  2, 8, 25,  65, 2, 122)
),
 
((0, 6, 8, 9, 1, 3),
(348, 3, 3, 4, 50, 0),
(451, 3, 3, 4, 5, 50),
(811, 3, 3, 4, 5, 50),
(151, 3, 3, 4, 50, 0),
(818, 1, 6, 3, 50, 0),
(444, 0, 2, 8, 50, 0),
(717, 0, 2, 8, 50, 0),
(883, 0, 2, 8, 50, 0),
(453, 4, 2, 4, 50, 0),
(834, 4, 52, 4, 5, 0),
(388, 1, 6, 3, 50, 0),
(833, 0, 2, 8, 50, 0),
),
 
((0, 6, 8, 9, 1, 4),
(1348, 3, 13, 4, 0, 25),
(2818, 1, 6, 3, 0, 15),
(1444, 0, 2, 8, 10, 10),
(1883, 0, 2, 8, 10, 1),
(1453, 4, 2, 4, 10, 1),
(2388, 1, 6, 3, 20 ,1),
(1833, 0, 2, 8, 20, 25),
(7171, 0, 2, 8, 10, 50)
)]
     

font_a = "/usr/share/fonts/truetype/roboto/Roboto-Bold.ttf"
font_b = "/usr/share/fonts/truetype/roboto/Roboto-Bold.ttf"
font_c = "/usr/share/fonts/truetype/roboto/Roboto-Bold.ttf"

#Pygame expects 16 bit integers defining the X and Y coordinates sampled from the sine wave
bits = 16

#the number of channels specified here is NOT 
#the channels talked about here http://www.pygame.org/docs/ref/mixer.html#pygame.mixer.get_num_channels


#We're going to play 44100 samples per second
pygame.mixer.pre_init(44100, -bits, 2)
pygame.init()
pygame.mixer.init()

#Make sure we take samples at the same rate we want to play them
sample_rate = 44100

note_duration = 0.125

def get_next(chain):
  (order, row) = get_highest_order_with_match(chain)
  #print order
  #print row
  column = get_winning_column(int(order), int(row))  

  note = chains[order][0][column]            
  return note

def get_winning_column(order, row):
  odds = chains[order][row]
  all_poss = 0
  for p in range(1, len(odds)):
    all_poss = all_poss + odds[p]
	
  seed = int(math.ceil(random.random() * all_poss))
  winner_number = 0
  lowest_match = 0
  range_start = 0
  range_end = 0 
  for x in range(1, len(odds)):
          
    range_end = range_end + odds[x]
    print str(range_start) + "-" + str(range_end)
    if (seed >= range_start and seed <= range_end):
      return x
    range_start = range_start + odds[x]
  
def get_highest_order_with_match(chain):
  chain_string = ""
  for x in range(0, len(chain)):
    chain_string = chain_string + str(chain[x])
	
  if (len(chain_string) <= 10):
    chain_string = "XXXXXXXXXX" + chain_string
 
  for i in range(len(chains) - 1, -1, -1):
    for j in range(len(chains[i]) - 1, 0, -1):

      chain_string = chain_string[len(chain_string)-len(str(chains[i][j][0])):len(chain_string)]

      if (chain_string == str(chains[i][j][0])):
        return i, j  

  return "", ""   		

def get_index(value, tupple):
  return "foo2"

def generate_sound(frequency, duration):
	#Figure out how many samples we need to take, use numpy library to create teh sample array
	n_samples = int(round(duration*sample_rate))
	buf = numpy.zeros((n_samples, 2), dtype = numpy.int16)
	max_sample = 2**(bits - 1) - 1

	for s in range(n_samples):
		t = float(s)/sample_rate    # time in seconds
		frequency_l = frequency;
		frequency_r = frequency;
		#grab the x-coordinate of the sine wave at a given time, while constraining the sample to what our mixer is set to with "bits"
		buf[s][0] = int(round(max_sample*math.sin(2*math.pi*frequency_l*t)))    # left
		buf[s][1] = int(round(max_sample*math.sin(2*math.pi*frequency_r*t)))    # right
	#Play the sound
	sound = pygame.sndarray.make_sound(buf)
	return sound;

our_sounds=[generate_sound(220, note_duration),
generate_sound(261.64 , note_duration),
generate_sound(261.6 , note_duration),
generate_sound(293.66 , note_duration),
generate_sound(329.63, note_duration),
generate_sound(349.23 , note_duration),
generate_sound(392  , note_duration),
generate_sound(440, note_duration),
generate_sound(493.88, note_duration),
generate_sound(523.25 , note_duration),
generate_sound(587.33 , note_duration),
generate_sound(659.26 , note_duration),
generate_sound(698.46, note_duration),
generate_sound(783.99 , note_duration),
generate_sound(880 , note_duration),
generate_sound(987.77 , note_duration),
generate_sound(1046.50 , note_duration),
generate_sound(1174.66 , note_duration),
generate_sound(1318.51 , note_duration),
generate_sound(1396.91  , note_duration),
generate_sound(1567.98 , note_duration),
generate_sound(1760, note_duration)]  	

our_colours=[(204,0,0),
(204,0,51),
(204,0,102),
(204,0,153),
(204,0,204),
(204,0,255),
(204,51,0),
(204,51,51),
(204,51,102),
(204,51,153),
(204,51,204),
(204,51,255),
(204,51,255),
(204,102,51),
(204,102,102),
(204,102,153),
(204,102,204),
(204,102,255),
(204,153,0),
(204,153,51),
(204,153,102),
(204,153,153)]  	


our_notes=['A3',
'B3',
'C3',
'D3',
'E3',
'F3',
'G3',
'A4',
'B4',
'C4',
'D4',
'E4',
'F4',
'G4',
'A5',
'B5',
'C5',
'D5',
'E5',
'F5',
'G5',
'A5']

def play(sound, sound1):
	chan1 = pygame.mixer.find_channel()
        chan1.queue(our_sounds[int(sound)])
#        chan1.queue(our_sounds[int(sound1)])

#	chan2 = pygame.mixer.find_channel()	
#        for x in range(0, random.randint(0, 50)):
#        chan2.queue(our_sounds[int(sound + random.randint(3, 4))])

#        chan2.queue(our_sounds[int(sound1 + random.randint(3, 4))])

	chan5 = pygame.mixer.find_channel()
	chan5.queue(our_sounds[int(sound + 5)])
#	chan5.queue(our_sounds[int(sound1 + 5)])
	
#	chan3 = pygame.mixer.find_channel()
#        for x in range(0, random.randint(0, 50)):
#                chan3.queue(our_sounds[int(sound + 5)])
	
	chan4 = pygame.mixer.find_channel()
#        for x in range(0, random.randint(0, 50)):
        chan4.queue(our_sounds[int(sound + 7)])
#        chan4.queue(our_sounds[int(sound1 + 7)])

        time.sleep(0.125)                        
#Pitch meaure START
#Use GPIO pin numbers to address the pins
GPIO.setmode(GPIO.BCM)

#Convenience handles for the pin numbers
TRIG_pitch = 23 
ECHO_pitch = 24

GPIO.setup(TRIG_pitch,GPIO.OUT)
GPIO.setup(ECHO_pitch,GPIO.IN)

def get_next(chain):
  (order, row) = get_highest_order_with_match(chain)
  #print order
  #print row
  column = get_winning_column(int(order), int(row))  

  note = chains[order][0][column]            
  return note
          
def get_winning_column(order, row):
  odds = chains[order][row]
  all_poss = 0
  for p in range(1, len(odds)):
    all_poss = all_poss + odds[p]
	
  seed = int(math.floor(random.random() * (all_poss - 1)))
  winner_number = 0
  lowest_match = 0
  range_start = 0
  range_end = 0
#  print "seed= " + str(seed)  
  for x in range(1, len(odds)):
    range_end = range_end + odds[x]
#    print str(range_start) + "-" + str(range_end)
    if (seed >= range_start and seed <= range_end):
      return x
    range_start = range_start + odds[x]
  
def get_highest_order_with_match(chain):
  chain_string = ""
  for x in range(0, len(chain)):
    chain_string = chain_string + str(chain[x])
	
  if (len(chain_string) <= 10):
    chain_string = "XXXXXXXXXX" + chain_string
 
  for i in range(len(chains) - 1, -1, -1):
    for j in range(len(chains[i]) - 1, 0, -1):

      chain_string = chain_string[len(chain_string)-len(str(chains[i][j][0])):len(chain_string)]

      if (chain_string == str(chains[i][j][0])):
        return i, j  

  return "", ""   		

def get_index(value, tupple):
  return "foo2"
	
   
def main():
  chain = []
  chain1 = []
  note = 1
  note1 = 3

  current_note_length = note_length[0]  

  
  for i in range(0, 10000):
    t_start = datetime.datetime.now()
    chain.append(note)
    chain1.append(note1)
#    print "==="
    note = get_next(chain)
    note1 = get_next(chain1)
    if (note >= 100):
      play(note, note1)          

    seed = int(math.floor(random.random() * (8 - 1)))
    play(seed, seed)

#    note1 = get_next(chain1)

#    if (note1 <= 11):
#      play(note1)
        	
	#silence
#    if (note == 11):
#      time.sleep(0.125)
	#Increase note length
#    if (note == 10):
#      if ((float(current_note_length) + 0.250) <= 1.500):      
#        current_note_length = float(current_note_length) + 0.250
	#Decrease note length
 #   if (note == 11):
#      if ((float(current_note_length) - 0.250) == 0.000):      
        
#		current_note_length = float(current_note_length) - 0.250
  
	#Change key
#    if (note == 11):
#      a = ""	  

#    print current_note_length	  
	
    chain_string = ""
    for x in range(0, len(chain)):
      note_string = str(chain[x])  
	  #Tempo down
      note_string = note_string.replace('10', 'Downtempo')  
      #Key change	  
      note_string = note_string.replace('11', 'KeyChange')        
      note_string = note_string.replace('1', 'I')
      note_string = note_string.replace('2', 'II')
      note_string = note_string.replace('3', 'III')
      note_string = note_string.replace('4', 'IV')
      note_string = note_string.replace('5', 'V')
      note_string = note_string.replace('6', 'VI')
      note_string = note_string.replace('7', 'VII')
      #Random tempo change     
      note_string = note_string.replace('8', 'VIII')
      #Tempo up
      note_string = note_string.replace('9', 'Silence')

      note_string = note_string + " (" + str(current_note_length) + ")"
      chain_string = chain_string + " " + note_string
#    print chain_string
    t_end = datetime.datetime.now()
    delta_measure = t_start - t_end
    time_to_sleep = (150000 + (delta_measure.total_seconds() * 1000 * 1000)) / 1000000
    if (time_to_sleep >= 0):
      time.sleep(time_to_sleep)
#    print str(delta_measure.total_seconds() * 1000)
    
    

def draw(font, text_colour, background_colour, the_text, screen, offset, background, is_top, font_size):
	# Fill background
	#background = pygame.Surface(screen.get_size())
	#background = background.convert()
	background.fill(background_colour)

	# Display some text
	font = pygame.font.Font(font, font_size)
	text = font.render(the_text, 1, text_colour)
	textpos = text.get_rect()
	textpos.centerx = background.get_rect().left
	textpos.centery = background.get_rect().top
	background.blit(text, (textpos.centerx, textpos.centery))

	# Blit everything to the screen
	if is_top:
		screen.blit(background, (0, 0))
	if not is_top:
		screen.blit(background, (0, screen.get_height() / 2))

	pygame.display.flip() 



if __name__ == '__main__': main()
