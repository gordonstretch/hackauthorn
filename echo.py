import RPi.GPIO as GPIO
import time
import math
import thread
import multiprocessing
from multiprocessing import Process

import pygame
from pygame.locals import *

import math
import numpy
import os
import time


#We need this because even though we're not outputting anything to the screen using pygame SDL_VIDEODRIVER needs a value otherwise pygame fails to init
os.environ["SDL_VIDEODRIVER"] = "dummy"

#Same with screen size as above
size = (1366, 720)

#Pygame expects 16 bit integers defining the X and Y coordinates sampled from the sine wave
bits = 16

#the number of channels specified here is NOT 
#the channels talked about here http://www.pygame.org/docs/ref/mixer.html#pygame.mixer.get_num_channels


#We're going to play 44100 samples per second
pygame.mixer.pre_init(44100, -bits, 2)
pygame.init()
pygame.mixer.init()

#Simple example of how to play a wav file
sounda = pygame.mixer.Sound("/usr/share/kde4/apps/kolf/sounds/blackhole.wav")
sounda.play()

#Setup the display - required but we don't care as we're not using the display just the sound
_display_surf = pygame.display.set_mode(size, pygame.HWSURFACE | pygame.DOUBLEBUF)

#Make sure we take samples at the same rate we want to play them
sample_rate = 44100

def play(frequency, duration, rest):
  #Figure out how many samples we need to take, use numpy library to create teh sample array
  n_samples = int(round(duration*sample_rate))
  buf = numpy.zeros((n_samples, 2), dtype = numpy.int16)
  max_sample = 2**(bits - 1) - 1

  for s in range(n_samples):
      t = float(s)/sample_rate    # time in seconds
      frequency_l = frequency;
      frequency_r = frequency;
      #grab the x-coordinate of the sine wave at a given time, while constraining the sample to what our mixer is set to with "bits"
      buf[s][0] = int(round(max_sample*math.sin(2*math.pi*frequency_l*t)))    # left
      buf[s][1] = int(round(max_sample*math.sin(2*math.pi*frequency_r*t)))    # right
  #Play the sound
  sound = pygame.sndarray.make_sound(buf)
  chan1 = pygame.mixer.find_channel()
  chan1.queue(sound)
  #sound.play()
  time.sleep(duration + rest)

while 0 == 0:
#Pitch meaure START
  #Use GPIO pin numbers to address the pins
  GPIO.setmode(GPIO.BCM)

  #Convenience handles for the pin numbers
  TRIG_pitch = 23 
  ECHO_pitch = 24
 
  #print "Distance Measurement In Progress"
  t_start = round(time.time() * 1000)
  #Tell the library that the Trigger pin is out (we send the signal) and Eacho  is in (we read the signal)
  GPIO.setup(TRIG_pitch,GPIO.OUT)
  GPIO.setup(ECHO_pitch,GPIO.IN)
  
  #Make sure the trigger is off (False == GPIO.LOW)
  GPIO.output(TRIG_pitch, False)
#  print "Waiting For Sensor To Settle"
  time.sleep(0.25)

  #Make the trigger fire
  GPIO.output(TRIG_pitch, True)
  time.sleep(0.00001)
  #Stop the trigger firing
  GPIO.output(TRIG_pitch, False)

  #To be honest I'l still trying to figure ut exactly how this works :-)
  while GPIO.input(ECHO_pitch)==0:
    pulse_start_pitch = time.time()
  
  while GPIO.input(ECHO_pitch)==1:
    pulse_end_pitch = time.time()

  pulse_duration_pitch = pulse_end_pitch - pulse_start_pitch
  distance_pitch = pulse_duration_pitch * 17150
  distance_pitch = math.floor(round(distance_pitch, 0)) 

#  print "Distance:",distance_pitch,"cm"
#  print "note:",distance 

  t_end = round(time.time() * 1000);
#  print distance_pitch
  GPIO.cleanup()


#Pitch measure end
  #Use GPIO pin numbers to address the pins
  GPIO.setmode(GPIO.BCM)

  TRIG_duration = 27
  ECHO_duration = 22

  GPIO.setup(TRIG_duration,GPIO.OUT)
  GPIO.setup(ECHO_duration,GPIO.IN)

  
  #Duration meaure START
  
  #Make sure the trigger is off (False == GPIO.LOW)
  GPIO.output(TRIG_duration, False)
#  print "Waiting For Sensor To Settle"
  time.sleep(0.25)

  #Make the trigger fire
  GPIO.output(TRIG_duration, True)
  time.sleep(0.00001)
  #Stop the trigger firing
  GPIO.output(TRIG_duration, False)

  #To be honest I'l still trying to figure ut exactly how this works :-)
  while GPIO.input(ECHO_duration)==0:
    pulse_start_duration = time.time()
  
  while GPIO.input(ECHO_duration)==1:
    pulse_end_duration = time.time()

  distance_duration = pulse_end_duration - pulse_start_duration
  distance_duration = distance_duration * 17150
  distance_duration = math.floor(round(distance_duration, 0)) 

  #print "Distance duration:",distance_duration,"cm"
#  print "note:",distance 

  t_end = round(time.time() * 1000);
  print "Actual emasureing time: " + str(int(t_start - t_end))
  print "Time to sleep         : " + str(int(0.250 - ((t_start - t_end) / 1000)))
  time.sleep(0.250 - ((t_start - t_end) / 1000))
  t_real_end = round(time.time() * 1000);
  print "total time taken:     : " + str(int(t_start - t_real_end))  
  print "sensor 1 measurement  : " + str(distance_duration)
  print "sensor 2 measurement  : " + str(distance_pitch)
#  GPIO.cleanup()
#Duration measure end 

  if distance_duration <= 30 and distance_duration >= 6:
    distance_duration = math.floor((distance_duration - 6) / 2)
#    print distance_duration
    frequency1 = 0
    if distance_duration == 1:
      frequency1 = 220
    if distance_duration == 2:
      frequency1 = 233
    if distance_duration == 3:
      frequency1 = 257
    if distance_duration == 4:
      frequency1 = 261.5
    if distance_duration == 5:
      frequency1 = 277
    if distance_duration == 6:
      frequency1 = 293.5
    if distance_duration == 7:
      frequency1 = 311
    if distance_duration == 8:
      frequency1 = 329.5
    if distance_duration == 9:
      frequency1 = 349
    if distance_duration == 10:
      frequency1 = 370
    if distance_duration == 11:
      frequency1 = 392
    if distance_duration == 12:
      frequency1 = 415
      
  
  if distance_pitch <= 30 and distance_pitch >= 6:
    distance_pitch = math.floor((distance_pitch - 6) / 2)
#    print distance_pitch
    frequency = 0
    if distance_pitch == 1:
      frequency = 440
    if distance_pitch == 2:
      frequency = 466
    if distance_pitch == 3:
      frequency = 494
    if distance_pitch == 4:
      frequency = 523
    if distance_pitch == 5:
      frequency = 554
    if distance_pitch == 6:
      frequency = 587
    if distance_pitch == 7:
      frequency = 622
    if distance_pitch == 8:
      frequency = 659
    if distance_pitch == 9:
      frequency = 698
    if distance_pitch == 10:
      frequency = 740
    if distance_pitch == 11:
      frequency = 784
    if distance_pitch == 12:
      frequency = 830

  #  try:
  #    0 == 0

#    print int(distance_duration / 4)
     # p = Process(target=play, args = (frequency, 0.25, 0, ))
    #play(frequency, int(frequency1 / 4), 0.1)
    play(frequency, 0.25, 0.1)
    play(frequency1, 0.25, 0.1)

     # p.start()
      #p.join()	
##    except:
     # print "Cannot start new thread"
    #play(frequency, 0.25, 0.0) 

  

  GPIO.cleanup()
