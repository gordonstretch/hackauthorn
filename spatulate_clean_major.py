#!/usr/bin/env python
# from https://bitbucket.org/gordonstretch/hackauthorn

import pygame
import time
from pygame.locals import *

import RPi.GPIO as GPIO
import time
import math
import thread
import multiprocessing
from multiprocessing import Process
import random
import math
import numpy
import os
import datetime

font_a = "/usr/share/fonts/truetype/roboto/Roboto-Bold.ttf"
font_b = "/usr/share/fonts/truetype/roboto/Roboto-Bold.ttf"
font_c = "/usr/share/fonts/truetype/roboto/Roboto-Bold.ttf"

#Pygame expects 16 bit integers defining the X and Y coordinates sampled from the sine wave
bits = 16

#the number of channels specified here is NOT 
#the channels talked about here http://www.pygame.org/docs/ref/mixer.html#pygame.mixer.get_num_channels

#Make sure we take samples at the same rate we want to play them
sample_rate = 22050

pygame.mixer.pre_init(sample_rate, -bits, 1)
pygame.init()
pygame.mixer.init()

note_duration = 0.6

def generate_sound(frequency, duration):
    print "Generating note at frequency",frequency,"Hz"
    #Figure out how many samples we need to take, use numpy library to create teh sample array
    n_samples = int(round(duration*sample_rate))
    buf = numpy.zeros((n_samples), dtype = numpy.int16)
    max_sample = 2**(bits - 1) - 1

    w = 2*math.pi*frequency/sample_rate
    for t in range(n_samples):
        # grab the x-coordinate of the sine wave at a given time, while constraining
        # the sample to what our mixer is set to with "bits"
        buf[t] = int(round(max_sample*math.sin(w*float(t))))
    # Convert the numpy array into a sound object
    sound = pygame.sndarray.make_sound(buf)
    return sound;

our_sounds=[generate_sound(220,      note_duration),
            generate_sound(246.94,   note_duration),
            generate_sound(261.63,   note_duration),
            generate_sound(293.66,   note_duration),
            generate_sound(329.63,   note_duration),
            generate_sound(349.23,   note_duration),
            generate_sound(392 ,     note_duration),
            generate_sound(440,      note_duration),
            generate_sound(493.88,   note_duration),
            generate_sound(523.25,   note_duration),
            generate_sound(587.33,   note_duration),
            generate_sound(659.26,   note_duration),
            generate_sound(698.46,   note_duration),
            generate_sound(783.99,   note_duration),
            generate_sound(880,      note_duration),
            generate_sound(987.77,   note_duration),
            generate_sound(1046.50,  note_duration),
            generate_sound(1174.66,  note_duration),
            generate_sound(1318.51,  note_duration),
            generate_sound(1396.91 , note_duration),
            generate_sound(1567.98,  note_duration),
            generate_sound(1760,     note_duration)]    

our_colours=[(136,96,55),
             (175,175,216),
             (0,64,130),
             (167,176,181), 
             (63,122,233), 
             (0,80,46), 
             (206,32,41),
             (136,96,55),
             (175,175,216),
             (0,64,130),
             (167,176,181), 
             (63,122,233), 
             (0,80,46), 
             (206,32,41),
             (136,96,55),
             (175,175,216),
             (0,64,130),
             (167,176,181), 
             (63,122,233), 
             (0,80,46), 
             (206,32,41),
             (136,96,55)]       


our_notes=['A3',
           'B3',
           'C3',
           'D3',
           'E3',
           'F3',
           'G3',
           'A4',
           'B4',
           'C4',
           'D4',
           'E4',
           'F4',
           'G4',
           'A5',
           'B5',
           'C5',
           'D5',
           'E5',
           'F5',
           'G5',
           'A5']

def play(sound):
    chan1 = pygame.mixer.find_channel()
    chan1.play(our_sounds[int(sound)])
    #chan1.queue(our_sounds[int(sound)])

#    chan2 = pygame.mixer.find_channel()   
#    chan2.queue(our_sounds[int(sound + 3)])
#
#    chan5 = pygame.mixer.find_channel()
#    chan5.queue(our_sounds[int(sound + 5)])
#    
#    chan4 = pygame.mixer.find_channel()
#    chan4.queue(our_sounds[int(sound + 8)])
        
#Pitch measure START
#Use GPIO pin numbers to address the pins
GPIO.setmode(GPIO.BCM)

#Convenience handles for the pin numbers
TRIG_pitch = 23 
ECHO_pitch = 24

GPIO.setup(TRIG_pitch,GPIO.OUT)
GPIO.setup(ECHO_pitch,GPIO.IN)

def main():
        
    # Initialise screen
    pygame.init()
    screen = pygame.display.set_mode((0, 0), pygame.RESIZABLE)
    pygame.display.set_caption('The Synthetic Synesthesic Sonic Spatulas welcome YOU!')

    # Fill background
    background1 = pygame.Surface((screen.get_width(), screen.get_height() / 2))
    background1 = background1.convert()
    background1.fill((255,255,255))

    background2 = pygame.Surface((screen.get_width(), screen.get_height() / 2))
    background2 = background2.convert()
    background2.fill((255,255,255))

    # Blit everything to the screen
    screen.blit(background1, (0, 0))    
    screen.blit(background2, (0, screen.get_height() / 2))

    pygame.display.flip()

    GPIO.output(TRIG_pitch, False)
    time.sleep(1)

    note_string = "";
    while True:
        t_start = datetime.datetime.now()
        #  print "start time: " + int(t_start.total_seconds() * 1000)
        time.sleep(0.4)
        GPIO.output(TRIG_pitch, True)

        time.sleep(0.00002)

        GPIO.output(TRIG_pitch, False)

        while GPIO.input(ECHO_pitch)==0:
            pulse_start_pitch = time.time()

        while GPIO.input(ECHO_pitch)==1:
            pulse_end_pitch = time.time()

        pulse_duration_pitch = pulse_end_pitch - pulse_start_pitch
        distance_pitch = pulse_duration_pitch * 17150
        print "Distance = ",distance_pitch
        distance_pitch = math.floor(round(distance_pitch, 0)) 

        t_measure_end = datetime.datetime.now()     
        delta_measure = t_measure_end - t_start 
        #print "time to measure     :" + str(delta_measure.total_seconds() * 1000)
        time_to_sleep = (425000 - (delta_measure.total_seconds() * 1000 * 1000)) / 1000000
        if (time_to_sleep >= 0):
            time.sleep(time_to_sleep)
        #print "time to sleep =",str(time_to_sleep)
        t_play_start = datetime.datetime.now()  

        if distance_pitch <= 60 and distance_pitch >= 0:
            distance_pitch = math.floor((distance_pitch) / 4)
            note_string = our_notes[int(distance_pitch)] + " " + note_string  
            if (len(note_string) >= 42):
                note_string = note_string[0:39] 
                
            draw(font_a, (0,0,0), our_colours[int(distance_pitch)], note_string, screen, 0, background1, True, 88);
            draw(font_a, our_colours[int(distance_pitch)], (0,0,0), our_notes[int(distance_pitch)], screen, 0, background2, False, 176);
            #thread.start_new_thread(play, (int(distance_pitch),))
            play(int(distance_pitch))

        t_end = datetime.datetime.now()
        delta = t_end - t_start
        time_to_sleep = (550000 - (delta.total_seconds() * 1000 * 1000)) / 1000000
        #print "Time to sleep     : " + str(time_to_sleep)
        if (time_to_sleep >= 0):
            time.sleep(time_to_sleep)


def draw(font, text_colour, background_colour, the_text, screen, offset, background, is_top, font_size):
    # Fill background
    #background = pygame.Surface(screen.get_size())
    #background = background.convert()
    background.fill(background_colour)

    # Display some text
    font = pygame.font.Font(font, font_size)
    text = font.render(the_text, 1, text_colour)
    textpos = text.get_rect()
    textpos.centerx = background.get_rect().left
    textpos.centery = background.get_rect().top
    background.blit(text, (textpos.centerx, textpos.centery))

    # Blit everything to the screen
    if is_top:
        screen.blit(background, (0, 0))
    if not is_top:
        screen.blit(background, (0, screen.get_height() / 2))

    pygame.display.flip() 



if __name__ == '__main__':
    try:
        main()
    except:
        print "Tidying up"
        GPIO.cleanup()
        raise
