#!/usr/bin/python

import pygame
import time
from pygame.locals import *

import math
import thread
import multiprocessing
from multiprocessing import Process
import random
import numpy
import os
import datetime

font_a = "/usr/share/fonts/truetype/roboto/Roboto-Bold.ttf"
font_b = "/usr/share/fonts/truetype/roboto/Roboto-Bold.ttf"
font_c = "/usr/share/fonts/truetype/roboto/Roboto-Bold.ttf"

#Pygame expects 16 bit integers defining the X and Y coordinates sampled from the sine wave
bits = 16

#the number of channels specified here is NOT 
#the channels talked about here http://www.pygame.org/docs/ref/mixer.html#pygame.mixer.get_num_channels


#We're going to play 44100 samples per second
pygame.mixer.pre_init(44100, -bits, 2)
pygame.init()
pygame.mixer.init()

#Make sure we take samples at the same rate we want to play them
sample_rate = 44100

note_duration = 0.25

our_sounds=[]  	
our_colours=[]  	

first_order = [{1:{1:0.1,7:0.6,10:0.3}}, {2:{1:0.25,7:0.05,12:0.7}},{3:{1:0.7,3:0.0,12:0.3}}]
first_order = [(0,1,3,7), (1, 0.5,0.25,0.25), (3, 0.10,0.80,0.10), (7, 0.2, 0.4, 0.4)]


def generate_sound(frequency, duration):
	#Figure out how many samples we need to take, use numpy library to create teh sample array
	n_samples = int(round(duration*sample_rate))
	buf = numpy.zeros((n_samples, 2), dtype = numpy.int16)
	max_sample = 2**(bits - 1) - 1

	for s in range(n_samples):
		t = float(s)/sample_rate    # time in seconds
		frequency_l = frequency;
		frequency_r = frequency;
		#grab the x-coordinate of the sine wave at a given time, while constraining the sample to what our mixer is set to with "bits"
		buf[s][0] = int(round(max_sample*math.sin(2*math.pi*frequency_l*t)))    # left
		buf[s][1] = int(round(max_sample*math.sin(2*math.pi*frequency_r*t)))    # right
	#Play the sound
	sound = pygame.sndarray.make_sound(buf)
	return sound;

def play(sound):
	chan1 = pygame.mixer.find_channel()
        chan1.queue(our_sounds[int(sound)])

	chan2 = pygame.mixer.find_channel()	
        chan2.queue(our_sounds[int(sound + 2)])

	chan5 = pygame.mixer.find_channel()
	chan5.queue(our_sounds[int(sound + 4)])
	
	chan4 = pygame.mixer.find_channel()
        chan4.queue(our_sounds[int(sound + 8)])

def play_sound(sound):
	chan1 = pygame.mixer.find_channel()
        chan1.queue(sound)
        time.sleep(0.25)

def get_next_note(note):
        return note * (2 ** (1 / 12.0))

def main():


        current_note = 0;
        previous_note = 0;

        note = 220;
        for x in range(1,12):
                sound = generate_sound(note, note_duration)
                #play_sound(sound)
                #print str(note)+ "\n"
                our_sounds.append(sound)
                our_colours.append((233,233,233))
                note = get_next_note(note)


        while (0 == 0):

                previous_note = current_note
                current_note = get_next_note(current_note)
                #print first_order[current_note][current_note]
                play_sound(our_sounds[current_note])         

def get_next_note(note):
        for i in range(0, len(first_order)):
                print first_order[i]                 

def draw(font, text_colour, background_colour, the_text, screen, offset, background, is_top, font_size):
	# Fill background
	#background = pygame.Surface(screen.get_size())
	#background = background.convert()
	background.fill(background_colour)

	# Display some text
	font = pygame.font.Font(font, font_size)
	text = font.render(the_text, 1, text_colour)
	textpos = text.get_rect()
	textpos.centerx = background.get_rect().left
	textpos.centery = background.get_rect().top
	background.blit(text, (textpos.centerx, textpos.centery))

	# Blit everything to the screen
	if is_top:
		screen.blit(background, (0, 0))
	if not is_top:
		screen.blit(background, (0, screen.get_height() / 2))

	pygame.display.flip() 



if __name__ == '__main__': main()
