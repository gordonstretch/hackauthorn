import RPi.GPIO as GPIO
import time
import math
import thread
import multiprocessing
from multiprocessing import Process

import pygame
from pygame.locals import *

import math
import numpy
import os
import time
import datetime


#We need this because even though we're not outputting anything to the screen using pygame SDL_VIDEODRIVER needs a value otherwise pygame fails to init
os.environ["SDL_VIDEODRIVER"] = "dummy"

#Same with screen size as above
size = (1366, 720)

#Pygame expects 16 bit integers defining the X and Y coordinates sampled from the sine wave
bits = 16

#the number of channels specified here is NOT 
#the channels talked about here http://www.pygame.org/docs/ref/mixer.html#pygame.mixer.get_num_channels


#We're going to play 44100 samples per second
pygame.mixer.pre_init(44100, -bits, 2)
pygame.init()
pygame.mixer.init()

#Simple example of how to play a wav file
#sounda = pygame.mixer.Sound("/usr/share/kde4/apps/kolf/sounds/blackhole.wav")
#sounda.play()

#Setup the display - required but we don't care as we're not using the display just the sound
_display_surf = pygame.display.set_mode(size, pygame.HWSURFACE | pygame.DOUBLEBUF)

#Make sure we take samples at the same rate we want to play them
sample_rate = 44100

note_duration = 0.25





def generate_sound(frequency, duration):
  #Figure out how many samples we need to take, use numpy library to create teh sample array
  n_samples = int(round(duration*sample_rate))
  buf = numpy.zeros((n_samples, 2), dtype = numpy.int16)
  max_sample = 2**(bits - 1) - 1

  for s in range(n_samples):
      t = float(s)/sample_rate    # time in seconds
      frequency_l = frequency;
      frequency_r = frequency;
      #grab the x-coordinate of the sine wave at a given time, while constraining the sample to what our mixer is set to with "bits"
      buf[s][0] = int(round(max_sample*math.sin(2*math.pi*frequency_l*t)))    # left
      buf[s][1] = int(round(max_sample*math.sin(2*math.pi*frequency_r*t)))    # right
  #Play the sound
  sound = pygame.sndarray.make_sound(buf)
  return sound;

our_sounds=[generate_sound(440, note_duration),
generate_sound(466, note_duration),
generate_sound(494, note_duration),
generate_sound(523, note_duration),
generate_sound(554, note_duration),
generate_sound(587, note_duration),
generate_sound(622, note_duration),
generate_sound(659, note_duration),
generate_sound(698, note_duration),
generate_sound(740, note_duration),
generate_sound(784, note_duration),
generate_sound(830, note_duration)]  
  
def play(sound):
  chan1 = pygame.mixer.find_channel()
  chan1.queue(sound)

#Pitch meaure START
#Use GPIO pin numbers to address the pins
GPIO.setmode(GPIO.BCM)

#Convenience handles for the pin numbers
TRIG_pitch = 23 
ECHO_pitch = 24

GPIO.setup(TRIG_pitch,GPIO.OUT)
GPIO.setup(ECHO_pitch,GPIO.IN)
  
while 0 == 0:
  t_start = datetime.datetime.now()
#  print "start time: " + int(t_start.total_seconds() * 1000)
  GPIO.output(TRIG_pitch, False)
  time.sleep(0.25)
  
  GPIO.output(TRIG_pitch, True)

  time.sleep(0.00001)

  GPIO.output(TRIG_pitch, False)

  #To be honest I'l still trying to figure ut exactly how this works :-)

  while GPIO.input(ECHO_pitch)==0:
    pulse_start_pitch = time.time()

  while GPIO.input(ECHO_pitch)==1:
    pulse_end_pitch = time.time()

  pulse_duration_pitch = pulse_end_pitch - pulse_start_pitch
  distance_pitch = pulse_duration_pitch * 17150
  distance_pitch = math.floor(round(distance_pitch, 0)) 

  t_play_start = datetime.datetime.now()  
  
  if distance_pitch <= 30 and distance_pitch >= 6:
    distance_pitch = math.floor((distance_pitch - 6) / 2)
    print distance_pitch
    frequency = 0
    if distance_pitch == 1:
      frequency = 440
    if distance_pitch == 2:
      frequency = 466
    if distance_pitch == 3:
      frequency = 494
    if distance_pitch == 4:
      frequency = 523
    if distance_pitch == 5:
      frequency = 554
    if distance_pitch == 6:
      frequency = 587
    if distance_pitch == 7:
      frequency = 622
    if distance_pitch == 8:
      frequency = 659
    if distance_pitch == 9:
      frequency = 698
    if distance_pitch == 10:
      frequency = 740
    if distance_pitch == 11:
      frequency = 784
    if distance_pitch == 12:
      frequency = 830

  #  try:
  #    0 == 0 
     # p = Process(target=play, args = (frequency, 0.25, 0, ))
    thread.start_new_thread(play, (our_sounds[int(distance_pitch - 1)],))
  
  t_play_end = datetime.datetime.now()  
  
  t_end = datetime.datetime.now()
  
#  t_start = int(t_start.total_seconds() * 1000)
#  t_end = int(t_end.total_seconds() * 1000)
  delta = t_start - t_end
  delta_play = t_play_start - t_play_end
  print "Time to play          : " + str(delta_play.total_seconds() * 1000)
  
#  print "start time: " + str(t_end)
  print "Actual measuring time: " + str(delta.total_seconds() * 1000)
  time_to_sleep = (400000 + (delta.total_seconds() * 1000 * 1000)) / 1000000
  print "Time to sleep         : " + str(time_to_sleep)
  time.sleep(time_to_sleep)
  t_real_end = datetime.datetime.now()
  delta = t_start - t_real_end

  print "total time taken:     : " + str(delta.total_seconds() * 1000)  
  print "sensor 1 measurement  : " + str(distance_pitch)

  print "----"

GPIO.cleanup()
