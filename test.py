import pygame
from pygame.locals import *

import math
import numpy
import os
import time


os.environ["SDL_VIDEODRIVER"] = "dummy"

size = (1366, 720)

bits = 16
#the number of channels specified here is NOT 
#the channels talked about here http://www.pygame.org/docs/ref/mixer.html#pygame.mixer.get_num_channels

pygame.mixer.pre_init(44100, -bits, 2)
pygame.init()
pygame.mixer.init()

#sounda = pygame.mixer.Sound("/usr/share/kde4/apps/kolf/sounds/blackhole.wav")

#sounda.play()

_display_surf = pygame.display.set_mode(size, pygame.HWSURFACE | pygame.DOUBLEBUF)


duration = 0.25          # in seconds
#freqency for the left speaker
frequency_l = 61.74
#frequency for the right speaker
frequency_r = 82.41

#this sounds totally different coming out of a laptop versus coming out of headphones

sample_rate = 44100

n_samples = int(round(duration*sample_rate))

#setup our numpy array to handle 16 bit ints, which is what we set our mixer to expect with "bits" up above
while frequency_l < 800 and frequency_r < 800:

  n_steps = 2 

  buf = numpy.zeros((n_samples, 2), dtype = numpy.int16)
  max_sample = 2**(bits - 1) - 1

  for s in range(n_samples):
      t = float(s)/sample_rate    # time in seconds
      frequency_l = frequency_l;
      frequency_r = frequency_r;
      #grab the x-coordinate of the sine wave at a given time, while constraining the sample to what our mixer is set to with "bits"
      buf[s][0] = int(round(max_sample*math.sin(2*math.pi*frequency_l*t)))        # left
      buf[s][1] = int(round(max_sample*math.sin(2*math.pi*frequency_r*t)))    # right

#  print "Foo"
  sound = pygame.sndarray.make_sound(buf)

#  print sound.get_length()

  sound.play()
  time.sleep(duration + 0.10)

 # print math.pow(math.pow(2, 0.0,  n_steps)
  print 2 ** (1. / 12)
  magic = 2 ** (1. /12)


  frequency_l = frequency_l * math.pow(magic, n_steps)

  frequency_r = frequency_r * math.pow(magic,  n_steps)
  print frequency_l
  print frequency_r

#play once, then loop forever

 # sound.play(loops = -1)


def play(frequency, duration, rest):
  n_samples = int(round(duration*sample_rate))
  buf = numpy.zeros((n_samples, 2), dtype = numpy.int16)
  max_sample = 2**(bits - 1) - 1

  for s in range(n_samples):
      t = float(s)/sample_rate    # time in seconds
      frequency_l = frequency;
      frequency_r = frequency;
      #grab the x-coordinate of the sine wave at a given time, while constraining the sample to what our mixer is set to with "bits"
      buf[s][0] = int(round(max_sample*math.sin(2*math.pi*frequency_l*t)))    # left
      buf[s][1] = int(round(max_sample*math.sin(2*math.pi*frequency_r*t)))    # right

  sound = pygame.sndarray.make_sound(buf)
  sound.play()
  time.sleep(duration + rest)

#play(400, 3, 1)
#play(200, 5, 2)
play(440, 0.75, 0.25)
play(392, 0.75, 0.25)
play(349.23, 0.75, 0.25)
play(329.63, 0.75, 0.25)
play(293.66, 0.75, 0.25)
play(261.63, 0.75, 0.25)
play(246.94, 0.75, 0.25)

play(220, 0.75, 0.25)
play(196, 0.75, 0.25)
play(174.61, 0.75, 0.25)
play(164.81, 0.75, 0.25)
play(146.83, 0.75, 0.25)
play(130.81, 0.75, 0.25)
play(123.47, 0.75, 0.25)
while 0 == 0:
  play(440, 0.25, 0.05)
  play(392, 0.25, 0.05)
  play(349.23, 0.25, 0.05)
  play(329.63, 0.25, 0.05)
  play(293.66, 0.25, 0.05)
  play(261.63, 0.25, 0.05)
  play(246.94, 0.25, 0.05)

  play(220, 0.25, 0.05)
  play(196, 0.25, 0.05)
  play(174.61, 0.25, 0.05)
  play(164.81, 0.25, 0.05)
  play(146.83, 0.25, 0.05)
  play(130.81, 0.25, 0.05)
  play(123.47, 0.25, 0.05)










# keep the sound playing forever, the quit event handling allows the pygame window to close without crashing
_running = True
while _running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            _running = False
            break

pygame.quit()

