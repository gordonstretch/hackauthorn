import random
import math

major_scale = "2-2-1-2-2-2-1"
minor_scale = "2-1-2-2-1-2-2"
harmonic_minro_scale = "2-1-2-2-1-3-1"
melodic_minor = "2-1-2-2-2-2-1"

key = "Amaj"
root = "220Hz"
note_length = [0.125, 0.25, 5, 0.75, 1]

current_note_length = note_length[3]

chains = [(
(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11),
(1, 0 ,3 ,0 ,13 ,5 ,0 ,22, 10, 10, 22, 0),
(2, 2 ,0 ,1 ,44 ,1 ,1 ,1, 1, 15, 12, 0),
(3, 1 ,1 ,0 ,1 ,2 ,1 ,1, 1, 1, 22, 0),
(4, 1 ,1 ,1 ,0 ,55 ,2 ,1, 1, 13, 15, 0),
(5, 1 ,1 ,31 ,1 ,0 ,1 ,4, 1, 14, 21, 0),
(6, 1 ,1 ,1 ,1 ,1 ,0 ,5, 2, 1, 33, 0),
(7, 1 ,1 ,12 ,11 ,1 ,1 ,0, 41, 11, 22, 0),
(8, 1 ,1 ,1 ,1 ,1 ,2 ,1, 0, 30, 42, 11),
(9, 1 ,0 ,1 ,0 ,0 ,3 ,1, 1, 1, 1, 1),
(10, 1 ,1 ,1 ,1 ,2 ,1 ,0, 1, 1, 1, 0),
(11, 1 ,1 ,1 ,1 ,1 ,2 ,1, 0, 22, 20, 0)),

((0, 3, 4, 8),
(15, 3, 3, 4),
(57, 1, 6, 3),
(71, 0, 2, 8),
(12, 0, 2, 8),
(56, 4, 2, 4),
(34, 1, 6, 3),
(93, 0, 2, 8),
(58, 4, 2, 4),
(38, 1, 6, 3),
(18, 0, 2, 8),
(81, 4, 2, 4),
(83, 1, 6, 3),
(85, 0, 2, 8)
),
 
((0, 6, 8, 9),
(348, 3, 3, 4),
(818, 1, 6, 3),
(444, 0, 2, 8),
(883, 0, 2, 8),
(353, 4, 2, 4),
(388, 1, 6, 3),
(833, 0, 2, 8),
)]
              
def get_next(chain):
  (order, row) = get_highest_order_with_match(chain)
  #print order
  #print row
  column = get_winning_column(int(order), int(row))  

  note = chains[order][0][column]            
  return note
          
def get_winning_column(order, row):
  odds = chains[order][row]
  all_poss = 0
  for p in range(1, len(odds)):
    all_poss = all_poss + odds[p]
	
  seed = int(math.ceil(random.random() * all_poss))
  winner_number = 0
  lowest_match = 0
  range_start = 0
  range_end = 0 
  for x in range(1, len(odds)):
    range_end = range_end + odds[x]
    if (seed >= range_start and seed <= range_end):
      return x
    range_start = range_start + odds[x]
  
def get_highest_order_with_match(chain):
  chain_string = ""
  for x in range(0, len(chain)):
    chain_string = chain_string + str(chain[x])
	
  if (len(chain_string) <= 10):
    chain_string = "XXXXXXXXXX" + chain_string
 
  for i in range(len(chains) - 1, -1, -1):
    for j in range(len(chains[i]) - 1, 0, -1):

      chain_string = chain_string[len(chain_string)-len(str(chains[i][j][0])):len(chain_string)]

      if (chain_string == str(chains[i][j][0])):
        return i, j  

  return "", ""   		

def get_index(value, tupple):
  return "foo2"
  
   
def main():
  chain = []
  note = 5
  current_note_length = note_length[0]  

  
  for i in range(0, 10000):
    chain.append(note)
    note = get_next(chain)
	
	#silence
    if (note == 8):
      a = ""
	#Increase note length
    if (note == 9):
      if ((float(current_note_length) + 0.250) <= 1.500):      
        current_note_length = float(current_note_length) + 0.250
	#Decrease note length
    if (note == 10):
      if ((float(current_note_length) - 0.250) == 0.000):      
        
		current_note_length = float(current_note_length) - 0.250
  
	#Change key
    if (note == 11):
      a = ""	  

    print current_note_length	  
	
    chain_string = ""
    for x in range(0, len(chain)):
      note_string = str(chain[x])  
	  #Tempo down
      note_string = note_string.replace('10', 'Downtempo')  
      #Key change	  
      note_string = note_string.replace('11', 'KeyChange')        
      note_string = note_string.replace('0', 'I') 
      note_string = note_string.replace('1', 'II')
      note_string = note_string.replace('2', 'III')
      note_string = note_string.replace('3', 'IV')
      note_string = note_string.replace('4', 'V')
      note_string = note_string.replace('5', 'VI')
      note_string = note_string.replace('6', 'VII')
      note_string = note_string.replace('7', 'VIII')
      #Random tempo change     
      note_string = note_string.replace('8', 'Silence')
      #Tempo up
      note_string = note_string.replace('9', 'UpTempo')

      note_string = note_string + " (" + str(current_note_length) + ")"
      chain_string = chain_string + " " + note_string
    print chain_string
              
if __name__ == "__main__":
    main()