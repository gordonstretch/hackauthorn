# The Spatulatron #
* This repository holds the code for a simple musical instrument that uses a cheap ultrasonic distance detector attached to a Raspberry PI to play notes in C Major, writing the note name and an associated colour to the screen.
* To setup the PI and the sensor you can follow this tutorial: https://www.modmypi.com/blog/hc-sr04-ultrasonic-range-sensor-on-the-raspberry-pi.
* Basically all we did was adjust the code in the tutorial and add the making muci bit and the output to the display. 
* Version 0.02a
* Credit to Nicholas Blachford, Rishit Adathiruthi, Peter Jones, Sally Neale and Gordon Stretch for the original hackauthorn project and Tim Clapp for helping out showing explaining the project to school children at an Ada Lovelace day event and optimising the original code.
* There is a lot of cruft here which explores other possiblities (different keys, different sounds, playing chords, Markov chains to generate music, etc.) in various states of development. 
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Install Pygame
* Run spatulate_clean_major.py